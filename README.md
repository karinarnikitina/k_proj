Telegram: @nautolan <br/>
Slack: Karina Nikitina <br/>

Машины размечены (`label.environment`) как `green` и `blue`. Первая версия файлов лежит на первой машине, вторая - на второй. Чтобы подготовить имаджи и поднять swarm, сначала надо запустить `init.sh` (из папки `~/qa_project` на менеджер ноде), который сбилдит и запушит имаджи + поднимет балансер и первую версию сервиса. <br/>

Далее можно запускать CICD:
- сервисы будут подниматься на машине `blue` с данными на ней. Т.е. используется те же имаджи, но в `docker-compose.blue.yml` указаны новые volume, constraints и переменные окружения. 
- (если бы эмбедер был реализован не из файлов, то его можно было бы не переподнимать, смотри описание папки v1 в ней)
- После поднятия делаем апдейт балансера и весь трафик пойдет на `blue` машину, при этом бекап версия будет на `green`. 
- Далее удаляем сервис на `green`. <br/>
**! CICD одноразовый, перед тем как запускать повторно, надо сначала поднять первую версию сервисов :'(** <br/>
**!Сейчас поднята версия V1, можно запускать CICD** <br/>
**Файлы в репозитории приведены для информации, они сложены на виртуальной машине и CICD выполняет все команды на удаленной машине.**


Индексы на машины попадают адресно и используются приложениями, которые запущены на тех же машинах. <br/>
Стратерия обновления blue/green. <br/>
Даунтайма нет, но минутку могут возвращаться результаты с разных машин из-за последовательного обновления реплик. <br/>
Здоровые приложения сервиса можно посмотреть с помощью `docker ps -f "name=qa" -f "health=healthy"`, на той машине, где они запущены. <br/>
Знаю, что надо спрятать все секретики, но пока без этого. <br/>

Картинка с описанием процесса (вероятно неточная и возможны разночтения). <br/>
<br/>

![image.png](./image.png)
