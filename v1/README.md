Папка v1 находится на первой машине, аналогичная папка v2 находится на второй машине. 
Содержание: 
- use_embeddings.pkl - эмбединги вопросов, не написан свой сервинг эмбедера, т.е. этот сервис может обрабатывать только те вопросы, которые перечислены в файле. Если такого запроса нет, то возвращает массив единиц. 
- clusters_centers_use.pkl - центры кластеров 
- clusters_use.json - списки вопросов, относящиеся к кластеру
- fake_knn.json - предрассчитанные (фейковые) ближайшие соседи, генерировался как:
```
fake_knn = {}
for cluster in data.keys():
    knn = {}
    n = len(data[cluster])
    for i in range(n):
        knn.update({i: random.sample(range(0, n), 10)})
    fake_knn.update({cluster:knn})
```
