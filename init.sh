#!/bin/bash
docker build -t 116.203.28.137:6000/balancer balancer
docker build -t 116.203.28.137:6000/embed embed
docker build -t 116.203.28.137:6000/qa qa

docker push 116.203.28.137:6000/balancer
docker push 116.203.28.137:6000/embed
docker push 116.203.28.137:6000/qa

docker stack deploy -c docker-compose.yml qa
