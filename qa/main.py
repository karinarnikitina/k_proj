from flask import Flask, jsonify, json, request
import numpy as np
import requests
import pickle
import os

HOST = '116.203.28.137'
PORT = os.environ.get("embedding_port")

with open('latest/clusters_centers_use.pkl', 'rb') as f:
    cluster_center = pickle.load(f)

with open('latest/clusters_use.json') as f:
    clusters_use = json.load(f)

with open('latest/fake_knn.json') as f:
    fake_knn = json.load(f)

app = Flask(__name__)

@app.route('/get_result', methods = ["GET"] )
def get_embed():
    data = request.get_json()
    name = data.get('query', '')

    embed_url = 'http://' + HOST + ':' + PORT + '/get_embedding'
    embedding = requests.get(embed_url, json={"query":name})
    embedding = np.array(embedding.json()['embedding'])

    cos_list = []
    for cluster in cluster_center.keys():
        center = cluster_center[cluster]
        cos_list.append(np.abs(np.dot(embedding, center)/(np.linalg.norm(embedding)*np.linalg.norm(center))))

    cluster = [*cluster_center.keys()][np.argmin(cos_list)]

    try:
        q_id  = data[str(cluster)].index(name)
    except:
        q_id = 0 #in case no such query in given cluster

    get_knn = fake_knn[str(cluster)][str(q_id)]

    suggections = []
    for key in get_knn:
        try:
            suggections.append(clusters_use[cluster][key])
        except:
            pass

    return jsonify({"suggections": suggections, "cluster":cluster})


if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 5000, threaded=True)
