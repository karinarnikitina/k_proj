from flask import Flask, jsonify, json, request
import numpy as np
import pickle


with open('latest/use_embeddings.pkl', 'rb') as f:
    queries = pickle.load(f)

app = Flask(__name__)

@app.route('/get_embedding', methods = ["GET"] )
def get_embed():
    data = request.get_json()
    name = data.get('query', '')
    try:
        embedding = queries[name]
    except:
        embedding = np.ones([1, 512]) #in case no precalculated embedding for query
    embedding = np.array(embedding)

    return jsonify({"embedding": embedding.tolist()})


if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 5000, threaded=True)


